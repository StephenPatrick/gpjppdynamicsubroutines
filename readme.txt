Compilation is the same as Tartarus2. (cd Project\ Code/Tartarus2; javac -cp "../:." *.java; java -cp "../:." Tartarus)

To adapt any existing gpjpp extension to use Dynamic Subroutines, make the following three modifications:

In your extension of GPRun, add to the GPAdfNodeSet a GPSeperatedNodeSet instead of a GPNodeSet for your initial functions and terminals.

Add the following or equivalent to your evaluate function in your extension of GPGene, or define evaluate in GPGene as the following and call super():

if (node instanceof GPSubroutine){
    if (!( (GPSubroutine) node).isFull())
        ( (GPSubroutine) node).addChildren(container);
    ( (TartGene)(node.getTree().get(0)) ).evaluate(cfg, gp, os);
}

Add the following variables to your .ini file:

UseDynamicSubroutines       (true, false) 
SubroutineSelectionMethod   (Random)
SubroutinesPerGeneration    (integer)
MaxSubroutineDepth          (integer)
MaxSubroutineArgs           (integer > 2)