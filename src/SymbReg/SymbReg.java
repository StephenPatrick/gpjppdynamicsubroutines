// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import gpjpp.*;

//Symbolic regression test
class SymbReg extends GPRun {

    //must override GPRun.createNodeSet to return 
    //  initialized set of functions & terminals
    protected GPAdfNodeSet createNodeSet(GPVariables cfg) {
        GPAdfNodeSet adfNs = new GPAdfNodeSet();

        if (cfg.UseADFs) {
            adfNs.reserveSpace(2);
            GPNodeSet ns0 = new GPNodeSet(6);
            GPNodeSet ns1 = new GPNodeSet(4);
            adfNs.put(0, ns0);
            adfNs.put(1, ns1);

            //RPB has 4 functions plus ADF
            ns0.putNode(new GPNode('+', "+", 2));
            ns0.putNode(new GPNode('-', "-", 2));
            ns0.putNode(new GPNode('*', "*", 2));
            ns0.putNode(new GPNode('%', "%", 2));
            ns0.putNode(new GPNode('A', "ADF0", 2));
            ns0.putNode(new GPNode('X', "x"));

            //ADF0 does only + and * functions
            ns1.putNode(new GPNode('+', "+", 2));
            ns1.putNode(new GPNode('*', "*", 2));
            ns1.putNode(new GPNode(1, "x1"));
            ns1.putNode(new GPNode(2, "x2"));

        } else {
            adfNs.reserveSpace(1);
            GPNodeSet ns0 = new GPNodeSet(5);
            adfNs.put(0, ns0);

            ns0.putNode(new GPNode('+', "+", 2));
            ns0.putNode(new GPNode('-', "-", 2));
            ns0.putNode(new GPNode('*', "*", 2));
            ns0.putNode(new GPNode('%', "%", 2));
            ns0.putNode(new GPNode('X', "x"));
        }

        return adfNs;
    }

    //must override GPRun.createPopulation to return 
    //  SymbReg-specific population
    protected GPPopulation createPopulation(GPVariables cfg, 
        GPAdfNodeSet adfNs) {
        return new SymbRegPopulation(cfg, adfNs);
    }

    //construct this test case
    SymbReg(String baseName) { super(baseName, true); }

    //main application function
    public static void main(String[] args) {

        //compute base file name from command line parameter
        String baseName;
        if (args.length == 1)
            baseName = args[0];
        else
            baseName = "symbreg";

        //construct the test case
        SymbReg test = new SymbReg(baseName);

        //run the test
        test.run();

        //make sure all threads are killed
        System.exit(0);
    }
}

//=====================================================================

//extend GPGene to evaluate equations

class SymbRegGene extends GPGene {

    //public null constructor required during stream loading only

    //this constructor called when new genes are created
    SymbRegGene(GPNode gpo) { super(gpo); }

    //this constructor called when genes are cloned during reproduction
    SymbRegGene(SymbRegGene gpo) { super(gpo); }

    //called when genes are cloned during reproduction
    protected Object clone() { return new SymbRegGene(this); }

    //ID routine isA() required only for streams

    //must override GPGene.createChild to create AntGene instances
    public GPGene createChild(GPNode gpo) { return new SymbRegGene(gpo); }

    //protected divide, here protected by IEEE floating point standards
    double divide(double x, double y) {
        //return NaN or INF if y = 0
        return x/y;
    }

    //called by SymbRegGP.evaluate() for each branch of each GP
    //evaluate works with or without ADFs
    double evaluate(double x, SymbRegGP gp, double arg0, double arg1) {
        double ret = 0.0; //avoid compiler error

        if (isFunction()) {
            switch (node.value()) {
                case '*':
                    ret = ((SymbRegGene)get(0)).evaluate(x, gp, arg0, arg1) *
                          ((SymbRegGene)get(1)).evaluate(x, gp, arg0, arg1);
                    break;

                case '+':
                    ret = ((SymbRegGene)get(0)).evaluate(x, gp, arg0, arg1) +
                          ((SymbRegGene)get(1)).evaluate(x, gp, arg0, arg1);
                    break;

                case '-':
                    ret = ((SymbRegGene)get(0)).evaluate(x, gp, arg0, arg1) -
                          ((SymbRegGene)get(1)).evaluate(x, gp, arg0, arg1);
                    break;

                case '%':
                    //use protected divide function
                    ret = divide(((SymbRegGene)get(0)).evaluate(x, gp, arg0, arg1),
                                 ((SymbRegGene)get(1)).evaluate(x, gp, arg0, arg1));
                    break;

                case 'A':
                    //ADF0 function call.  We have access to that
                    //  subtree, as the GP gave us a reference to itself as
                    //  parameter.  We first evaluate the subtrees, and then
                    //  call the adf with the parameters
                    double a0 = ((SymbRegGene)get(0)).evaluate(x, gp, arg0, arg1);
                    double a1 = ((SymbRegGene)get(1)).evaluate(x, gp, arg0, arg1);
                    ret = ((SymbRegGene)gp.get(1)).evaluate(x, gp, a0, a1);
                    break;

                default:
                    throw new RuntimeException("Undefined function type "+node.value());
            }

        } else { //is terminal
            switch (node.value()) {
                case 'X':
                    ret = x;
                    break;

                case 1:
                    ret = arg0;
                    break;

                case 2:
                    ret = arg1;
                    break;

                default:
                    throw new RuntimeException("Undefined terminal type "+node.value());
            }
        }

        //keep results in reasonable range (needed only when NaN not used)
        //if (ret > 1.0e30)
        //    ret = 1.0e30;
        //else if (ret < -1.0e30)
        //    ret = -1.0e30;

        return ret;
    }
}

//=====================================================================

class SymbRegGP extends GP {
    //define fitness cases
    final static int DATAPOINTS = 20;
    static double[] ques = new double[DATAPOINTS];
    static double[] answ = new double[DATAPOINTS];

    //define equation to be fitted
    static double equation(double x) {
        return (x*x*x*x + x*x*x + x*x + x);
    }

    //initialize test case values
    static {
        for (int i = 0; i < DATAPOINTS; i++) {
            //test points evenly distributed in -1.0..+1.0
            ques[i] = -1.0+(2.0/DATAPOINTS)*i;
            answ[i] = equation(ques[i]);
        }
    }

    //public null constructor required during stream loading only

    //this constructor called when new GPs are created
    SymbRegGP(int genes) { super(genes); }

    //this constructor called when GPs are cloned during reproduction
    SymbRegGP(SymbRegGP gpo) { super(gpo); }

    //called when GPs are cloned during reproduction
    protected Object clone() { return new SymbRegGP(this); }

    //must override GP.createGene to create SymbRegGene instances
    public GPGene createGene(GPNode gpo) { return new SymbRegGene(gpo); }

    //must override GP.evaluate to return standard fitness
    public double evaluate(GPVariables cfg) {
        double rawFitness = 0.0;

        for (int i = 0; i < DATAPOINTS; i++) {
            double result = ((SymbRegGene)get(0)).evaluate(ques[i], this, 0, 0);
            if (Double.isNaN(result) || Double.isInfinite(result)) {
                //set maximum error
                return 1.0E30;
            } else
                //add absolute error to rawFitness
                rawFitness += Math.abs(answ[i]-result);
        }
        double stdF = rawFitness;
        if (cfg.ComplexityAffectsFitness)
            //add 0.1 for optimum length (12)
            stdF += (double)length()/120.0;
        return stdF;
    }
}

//=====================================================================

//extend GPPopulation to create SymbReg GP trees

class SymbRegPopulation extends GPPopulation {

    //this constructor called when new populations are created
    SymbRegPopulation(GPVariables gpVar, GPAdfNodeSet adfNs) {
        super(gpVar, adfNs);
    }

    //populations are not cloned in standard runs
    //SymbRegPopulation(SymbRegPopulation gpo) { super(gpo); }
    //protected Object clone() { return new SymbRegPopulation(this); }

    //must override GPPopulation.createGP to create SymbRegGP instances
    public GP createGP(int numOfGenes) { return new SymbRegGP(numOfGenes); }
}
