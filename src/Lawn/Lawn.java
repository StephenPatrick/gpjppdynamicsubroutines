// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import java.util.Properties;
import gpjpp.*;

//Lawnmower test
class Lawn extends GPRun {

    //must override GPRun.createVariables to return lawn-specific variables
    protected GPVariables createVariables() { 
        return new LawnVariables(); 
    }

    //must override GPRun.createNodeSet to return 
    //  initialized set of functions & terminals
    protected GPAdfNodeSet createNodeSet(GPVariables cfg) {
        GPAdfNodeSet adfNs = new GPAdfNodeSet();

        //ADFs used regardless of cfg setting
        adfNs.reserveSpace(3);

        //use the cfg setting to switch operator sets
        if (cfg.UseADFs) {
            //minimal set
            GPNodeSet ns0 = new GPNodeSet(5);
            GPNodeSet ns1 = new GPNodeSet(3);
            GPNodeSet ns2 = new GPNodeSet(4);
            adfNs.put(0, ns0);
            adfNs.put(1, ns1);
            adfNs.put(2, ns2);

            //RPB
            ns0.putNode(new GPNode(LawnMower.LEFT, "left"));
            ns0.putNode(new GPNode(LawnMower.MOW, "mow"));
            ns0.putNode(new GPNode(LawnMower.PROGN, "progn", 2));
            ns0.putNode(new GPNode(LawnMower.ADF0, "adf0"));
            ns0.putNode(new GPNode(LawnMower.ADF1, "adf1"));
  
            //ADF0
            ns1.putNode(new GPNode(LawnMower.LEFT, "left"));
            ns1.putNode(new GPNode(LawnMower.MOW, "mow"));
            ns1.putNode(new GPNode(LawnMower.PROGN, "progn", 2));
  
            //ADF1
            ns2.putNode(new GPNode(LawnMower.LEFT, "left"));
            ns2.putNode(new GPNode(LawnMower.MOW, "mow"));
            ns2.putNode(new GPNode(LawnMower.PROGN, "progn", 2));
            ns2.putNode(new GPNode(LawnMower.ADF0, "adf0"));

        } else {
            //Koza's set
            GPNodeSet ns0 = new GPNodeSet(8);
            GPNodeSet ns1 = new GPNodeSet(5);
            GPNodeSet ns2 = new GPNodeSet(8);
            adfNs.put(0, ns0);
            adfNs.put(1, ns1);
            adfNs.put(2, ns2);

            //"rv" = random vector
            //"va" = vector add

            //RPB
            ns0.putNode(new GPNode(LawnMower.LEFT, "left"));
            ns0.putNode(new GPNode(LawnMower.MOW, "mow"));
            ns0.putNode(new GPNode(LawnMower.RV, "rv"));
            ns0.putNode(new GPNode(LawnMower.ADF0, "adf0"));
            ns0.putNode(new GPNode(LawnMower.ADF1, "adf1", 1));
            ns0.putNode(new GPNode(LawnMower.FROG, "frog", 1));
            ns0.putNode(new GPNode(LawnMower.VA, "va", 2));
            ns0.putNode(new GPNode(LawnMower.PROGN, "progn", 2));
  
            //ADF0
            ns1.putNode(new GPNode(LawnMower.LEFT, "left"));
            ns1.putNode(new GPNode(LawnMower.MOW, "mow"));
            ns1.putNode(new GPNode(LawnMower.RV, "rv"));
            ns1.putNode(new GPNode(LawnMower.VA, "va", 2));
            ns1.putNode(new GPNode(LawnMower.PROGN, "progn", 2));
  
            //ADF1
            ns2.putNode(new GPNode(LawnMower.ARG0, "arg0"));
            ns2.putNode(new GPNode(LawnMower.LEFT, "left"));
            ns2.putNode(new GPNode(LawnMower.MOW, "mow"));
            ns2.putNode(new GPNode(LawnMower.RV, "rv"));
            ns2.putNode(new GPNode(LawnMower.ADF0, "adf0"));
            ns2.putNode(new GPNode(LawnMower.FROG, "frog", 1));
            ns2.putNode(new GPNode(LawnMower.VA, "va", 2));
            ns2.putNode(new GPNode(LawnMower.PROGN, "progn", 2));
        }

        return adfNs;
    }

    //must override GPRun.createPopulation to return 
    //  lawn-specific population
    protected GPPopulation createPopulation(GPVariables cfg, 
        GPAdfNodeSet adfNs) {
        return new LawnPopulation(cfg, adfNs);
    }

    //construct this test case
    Lawn(String baseName) { super(baseName, true); }

    //main application function
    public static void main(String[] args) {

        //compute base file name from command line parameter
        String baseName;
        if (args.length == 1)
            baseName = args[0];
        else
            baseName = "lawn8";

        //construct the test case
        Lawn test = new Lawn(baseName);

        //run the test
        test.run();

        //make sure all threads are killed
        System.exit(0);
    }
}
