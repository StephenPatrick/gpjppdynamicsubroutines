// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import java.util.Properties;
import gpjpp.*;

//extension of GPVariables for mower-specific stuff

public class LawnVariables extends GPVariables {

    //number of cells in trail grid
    public int WorldHorizontal = 32;
    public int WorldVertical = 32;

    //mower used to evaluate all trees, created in LawnGP.evaluate()
    public LawnMower mower;

    //public null constructor required for stream loading
    public LawnVariables() { /*gets default values*/ }

    //variables are never cloned in standard runs
    //public LawnVariables(LawnVariables gpo) {
    //    super(gpo);
    //    WorldHorizontal = gpo.WorldHorizontal;
    //    WorldVertical = gpo.WorldVertical;
    //}
    //protected Object clone() { return new LawnVariables(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERVARIABLESID; }

    //create the robot used to follow the trail
    public void createMower() {
        mower = new LawnMower(WorldHorizontal, WorldVertical);
    }

    //get values from properties
    public void load(Properties props) {

        if (props == null)
            return;
        super.load(props);
        WorldHorizontal = getInt(props, "WorldHorizontal", WorldHorizontal);
        WorldVertical = getInt(props, "WorldVertical", WorldVertical);
    }

    //get values from a stream
    protected void load(DataInputStream is)
        throws ClassNotFoundException, IOException,
            InstantiationException, IllegalAccessException {

        super.load(is);
        WorldHorizontal = is.readInt();
        WorldVertical = is.readInt();
    }

    //save values to a stream
    protected void save(DataOutputStream os) throws IOException {

        super.save(os);
        os.writeInt(WorldHorizontal);
        os.writeInt(WorldVertical);
    }

    //write values to a text file
    public void printOn(PrintStream os, GPVariables cfg) {

        super.printOn(os, cfg);
        os.println("WorldHorizontal           = "+WorldHorizontal);
        os.println("WorldVertical             = "+WorldVertical);
    }
}
