// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.awt.Point;
import java.io.*;
import gpjpp.*;

//extend GPGene to evaluate lawnmowing

public class LawnGene extends GPGene {
    //set to negative values until initialized
    protected Point randomConstant = new Point(-1, -1);

    //public null constructor required during stream loading only
    public LawnGene() {}

    //this constructor called when new genes are created
    LawnGene(GPNode gpo) { super(gpo); }

    //this constructor called when genes are cloned during reproduction
    LawnGene(LawnGene gpo) { 
        super(gpo); 
        randomConstant.x = gpo.randomConstant.x;
        randomConstant.y = gpo.randomConstant.y;
    }

    //called when genes are cloned during reproduction
    protected Object clone() { return new LawnGene(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGENEID; }

    //must override GPGene.createChild to create LawnGene instances
    public GPGene createChild(GPNode gpo) { return new LawnGene(gpo); }

    static protected Point zero = new Point(0, 0);

    //called by LawnGP.evaluate() for main branch of each GP
    Point evaluate(LawnVariables cfg, LawnGP gp, Point arg0) {

        //stop if all is mowed
        if (cfg.mower.lawnMissed() == 0)
            return zero;

        Point arg;
        Point arg1;

        switch (node.value()) {

            case LawnMower.LEFT: 
                //turn left and return zero
                cfg.mower.left();
                return zero;

            case LawnMower.MOW:
                //mow and return zero
                cfg.mower.mow();
                return zero;

            case LawnMower.PROGN:
                //evaluate first child
                ((LawnGene)get(0)).evaluate(cfg, gp);
                //return evaluation from second child
                return ((LawnGene)get(1)).evaluate(cfg, gp);

            case LawnMower.ADF0:
                //no argument for ADF0
                return ((LawnGene)(gp.get(1))).evaluate(cfg, gp);

            case LawnMower.ADF1:
                if (cfg.UseADFs)
                    //no arguments for ADF1 in minimal set
                    return ((LawnGene)(gp.get(2))).evaluate(cfg, gp);
                else
                    //one argument for ADF1 in Koza's set
                    return ((LawnGene)(gp.get(2))).evaluate(cfg, gp, 
                        ((LawnGene)get(0)).evaluate(cfg, gp));

            case LawnMower.ARG0:
                //argument for ADF1
                return arg0;

            case LawnMower.FROG:
                //jump to new position and mow
                arg = ((LawnGene)get(0)).evaluate(cfg, gp);
                cfg.mower.frog(arg);
                return arg;

            case LawnMower.RV:
                //random position
                //  if a value has not yet been assigned, do it now
                if (randomConstant.x < 0) {
                    randomConstant.x = GPRandom.nextInt(cfg.WorldHorizontal);
                    randomConstant.y = GPRandom.nextInt(cfg.WorldVertical);
                }
                return randomConstant;

            case LawnMower.VA:
                arg =  ((LawnGene)get(0)).evaluate(cfg, gp);
                arg1 = ((LawnGene)get(1)).evaluate(cfg, gp);
                return new Point(
                    (arg.x+arg1.x) % cfg.WorldHorizontal, 
                    (arg.y+arg1.y) % cfg.WorldVertical);

            default:
                throw new RuntimeException("Undefined function type "+node.value());
        }
    }

    //alternate form of evaluate for convenience
    Point evaluate(LawnVariables cfg, LawnGP gp) {
        return evaluate(cfg, gp, zero);
    }

    //override GPGene.geneRep to show RV values
    public String geneRep() { 
        if (node.value() == LawnMower.RV)
            return "["+randomConstant.x+","+randomConstant.y+"]";
        else
            return super.geneRep();
    }
}
