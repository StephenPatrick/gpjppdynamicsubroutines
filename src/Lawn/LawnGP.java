// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import gpjpp.*;

//extend GP for lawn mowing
//class must be public for stream loading; otherwise non-public ok

public class LawnGP extends GP {

    //public null constructor required during stream loading only
    public LawnGP() {}

    //this constructor called when new GPs are created
    LawnGP(int genes) { super(genes); }

    //this constructor called when GPs are cloned during reproduction
    LawnGP(LawnGP gpo) { super(gpo); }

    //called when GPs are cloned during reproduction
    protected Object clone() { return new LawnGP(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGPID; }

    //must override GP.createGene to create LawnGene instances
    public GPGene createGene(GPNode gpo) { return new LawnGene(gpo); }

    //must override GP.evaluate to return standard fitness
    public double evaluate(GPVariables cfg) {

        LawnVariables lcfg = (LawnVariables)cfg;

        //create mower first time through
        if (lcfg.mower == null)
            lcfg.createMower();

        //prepare mower for mowing
        lcfg.mower.reset();

        //evaluate main tree
        ((LawnGene)get(0)).evaluate(lcfg, this);

        //fitness is the squares of lawn missed (best = 0.0)
        double stdF = lcfg.mower.lawnMissed();

        if (cfg.ComplexityAffectsFitness)
            //add length into fitness to promote small trees
            stdF += length()/1000.0;

        //return standard fitness
        return stdF;
    }

    //optionally override GP.printOn to print lawn-specific data
    public void printOn(PrintStream os, GPVariables cfg) {

        super.printOn(os, cfg);

        //re-evaluate mower and log actions
        /*
        LawnVariables lcfg = (LawnVariables)cfg;
        lcfg.mower.logTo(os);
        evaluate(cfg);
        lcfg.mower.logTo(null);
        */
    }

    //optionally override GP.drawOn to draw lawn-specific data
    public void drawOn(GPDrawing ods, String fnameBase, 
        GPVariables cfg) throws IOException {

        //store the result trees to gif files
        super.drawOn(ods, fnameBase, cfg);

        //re-evaluate mower and draw actions
        LawnVariables lcfg = (LawnVariables)cfg;
        lcfg.mower.drawOn(ods, fnameBase+"TRL");
        evaluate(cfg);
        //disable drawing and save last gif file
        lcfg.mower.drawOn(null, null);
    }

}
