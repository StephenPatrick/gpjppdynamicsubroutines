// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import java.awt.Point;
import java.awt.Color;
import gpjpp.GPDrawing;

//keep track of a mower's state while evaluating fitness
//not part of the GP hierarchy, but called upon in LawnGP and LawnGene

final public class LawnMower {

    //functions and terminals
    public final static int LEFT = 0;   //turn left
    public final static int MOW = 1;    //move forward and mow
    public final static int RV = 2;     //random vector
    public final static int ADF0 = 3;   //ADF function calls
    public final static int ADF1 = 4;
    public final static int FROG = 5;   //jump and mow there
    public final static int VA = 6;     //vector addition
    public final static int PROGN = 7;  //execute two arguments, return result from second
    public final static int ARG0 = 8;   //argument passed to ADF1

    //for adjusting graphic image of mower trail
    public static int pixelsPerSquare = 40;
    public static int pixelsGridTick = 8;
    public static int pixelsArrowWidth = 4;
    public static int pixelsArrowHeight = 8;
    public static int fontSize = 10;
    public static int maxGifsPerTrail = 9;

    //size of the trail
    protected int worldHorizontal;
    protected int worldVertical;

    //the world the mower is moving in
    protected boolean[][] world;               //updated as mower moves

    //current position of mower
    protected Point pos = new Point(0, 0);      //current position

    //direction the mower is facing (value 0-3) and energy left to move
    protected int direction;

    //number of squares mowed
    protected int mowed;

    //if non-null, log to this stream
    protected PrintStream logOs;
    protected int logCount;
    protected static final String[] dir = 
        {" east", "north", " west", "south"};

    //if non-null, draw mower trail on this surface
    protected GPDrawing ods;
    protected String fname;
    protected int moveCounter;
    protected int gifCounter;
    protected boolean[][] blank;

    public LawnMower(int worldH, int worldV) {

        if ((worldH < 1) || (worldV < 1))
            throw new RuntimeException("Invalid world size");
        worldHorizontal = worldH;
        worldVertical = worldV;

        world = new boolean[worldHorizontal][worldVertical];
        blank = new boolean[worldHorizontal][worldVertical];
    }

    void reset() {
        //reset mow map
        for (int x = 0; x < worldHorizontal; x++)
            for (int y = 0; y < worldVertical; y++)
                world[x][y] = true;

        mowed = 0;
        direction = 3;     //South
        pos.x = 0;         //Northwest corner of world
        pos.y = 0;
    }

    public int lawnMissed() {
        return (worldHorizontal*worldVertical-mowed);
    }

    public void frog(Point v) {
        int newx = (pos.x+v.x) % worldHorizontal;
        int newy = (pos.y+v.y) % worldVertical;
        if (logOs != null)
            log("frog", newx, newy);
        if (ods != null) {
            //don't draw null jumps
            if ((pos.x != newx) || (pos.y != newy)) {
                checkArrow(newx, newy);
                drawArrow(pos.x, pos.y, newx, newy, true);
            }
        }
        pos.x = newx;
        pos.y = newy;
        mowArea();
    }

    public void left() {
        if (logOs != null)
            log("left", pos.x, pos.y);
        direction = (direction+1) % 4;
    }

    int sign(int i) { return i < 0? -1 : +1; }

    //move the mower forward and mow that area
    public void mow() {

        int newx = pos.x;
        int newy = pos.y;
        switch (direction) {
            case 0: //East
                newx = (newx+1) % worldHorizontal;
                break;

            case 1: //North
                newy = (newy+worldVertical-1) % worldVertical;
                break;

            case 2: // West
                newx = (newx+worldHorizontal-1) % worldHorizontal;
                break;

            case 3: // South
                newy = (newy+1) % worldVertical;
                break;

            default:
                throw new RuntimeException("Bad direction");
        }
      
        if (logOs != null)
            log(" mow", newx, newy);
        if (ods != null) {
            checkArrow(newx, newy);
            if (Math.abs(pos.x-newx) > 1) {
                //wrapped horizontally
                int s = sign(pos.x-newx);
                drawArrow(pos.x, pos.y, pos.x+s, pos.y, false);
                drawArrow(newx-s, pos.y, newx, pos.y, false);
            } else if (Math.abs(pos.y-newy) > 1) {
                //wrapped vertically
                int s = sign(pos.y-newy);
                drawArrow(pos.x, pos.y, pos.x, pos.y+s, false);
                drawArrow(pos.x, newy-s, pos.x, newy, false);
            } else
                drawArrow(pos.x, pos.y, newx, newy, false);
        }
        pos.x = newx;
        pos.y = newy;
        mowArea();
    }

    //mow the area at current mower position
    protected void mowArea() {
        if (world[pos.x][pos.y]) {
            mowed++;
            world[pos.x][pos.y] = false;
        }
    }

    //=============== for text trail logging

    void logTo(PrintStream os) { 
        logOs = os; 
        if (os != null) {
            logCount = 0;
            os.println("step position  dir oper target missed");
                      //cccc [xx,yy] ddddd frog unmowed mmmmm
        }
    }

    String formatInt(int i, int width) {
        String s = String.valueOf(i);
        while (s.length() < width)
            s = " "+s;
        return s;
    }

    void log(String opName, int newx, int newy) {
        logOs.print(formatInt(logCount++, 4));
        logOs.print(" ["+formatInt(pos.x, 2)+",");
        logOs.print(formatInt(pos.y, 2)+"] ");
        logOs.print(dir[direction]+" ");
        logOs.print(opName);
        if (world[newx][newy])
            logOs.print(" unmowed");
        else
            logOs.print("   mowed");
        logOs.println(formatInt(lawnMissed(), 5));
    }

    //=============== for graphic trail drawing

    //return pixel position for top or left of mowing square
    int mpos(int x) { return (x+1)*pixelsPerSquare; }

    //return pixel position for center of mowing square
    int cpos(int x) { return mpos(x)+pixelsPerSquare/2; }

    //return x pixel position for text in mowing square
    int txpos(int x) { return mpos(x)+2; }

    //return y pixel position for text in mowing square
    int typos(int y) { return mpos(y)+2+ods.as; }

    //draw the moveCounter text string to the specified square
    void drawCounter(int x, int y, boolean frogged) {
        String disp = Integer.toString(moveCounter);
        if (frogged)
            disp = disp+"f";
        ods.getGra().drawString(disp, txpos(x), typos(y));
    }

    //start a new gif file if x,y has already been written to
    void checkArrow(int x, int y) {

        if (!blank[x][y]) {
            //have already drawn to cell, start a new drawing
            try {
                ods.writeGif(fname+gifCounter+".gif");
            } catch (IOException e) {
                //disable drawing if error occurs
                ods = null;
                return;
            }
            if (++gifCounter > maxGifsPerTrail) {
                //disable drawing if maximum files already created
                ods = null;
                return;
            }
            prepImage();
        }
        blank[x][y] = false;
    }

    void drawArrow(int x, int y, int newx, int newy, boolean frogged) {

        if (ods == null)
            return;

        //compute and draw connecting line
        int x0 = cpos(x);
        int y0 = cpos(y);
        int x1 = cpos(newx);
        int y1 = cpos(newy);
        ods.getGra().drawLine(x0, y0, x1, y1);

        //compute angle of line and trig functions needed for arrowhead
        double theta = Math.atan2(y0-y1, x1-x0); //y-coordinate inverted
        double cosTheta = Math.cos(theta);
        double sinTheta = Math.sin(theta);
        double hCos = pixelsArrowHeight*cosTheta;
        double hSin = pixelsArrowHeight*sinTheta;
        double wCos = pixelsArrowWidth*cosTheta;
        double wSin = pixelsArrowWidth*sinTheta;

        //arrowhead is a rotated three-point filled polygon
        int[] xPoints = new int[3];
        int[] yPoints = new int[3];
        xPoints[0] = x1;
        yPoints[0] = y1;
        xPoints[1] = (int)Math.round(x1-hCos+wSin);
        yPoints[1] = (int)Math.round(y1+wCos+hSin);
        xPoints[2] = (int)Math.round(x1-hCos-wSin);
        yPoints[2] = (int)Math.round(y1-wCos+hSin);
        ods.getGra().fillPolygon(xPoints, yPoints, 3);

        //display counter and frog symbol only for points within lawn
        if ((newx >= 0) && (newx < worldHorizontal) &&
            (newy >= 0) && (newy < worldVertical)) {
            moveCounter++;
            drawCounter(newx, newy, frogged);
        }
    }

    protected void drawGrid(int w, int h) {

        int t = pixelsGridTick/2;
        for (int x = pixelsPerSquare; x < w; x += pixelsPerSquare)
            for (int y = pixelsPerSquare; y < h; y += pixelsPerSquare) {
                ods.getGra().drawLine(x-t, y, x+t, y);
                ods.getGra().drawLine(x, y-t, x, y+t);
            }

        //fill unmowed squares with color
        ods.getGra().setColor(Color.yellow);
        for (int x = 0; x < worldHorizontal; x++)
            for (int y = 0; y < worldVertical; y++)
                if (world[x][y])
                    ods.getGra().fillRect(mpos(x)+1, mpos(y)+1, 
                        pixelsPerSquare-1, pixelsPerSquare-1);
        ods.getGra().setColor(Color.black);
    }

    protected void prepImage() {

        //prepare drawing surface of required size
        int w = (worldHorizontal+2)*pixelsPerSquare;
        int h = (worldVertical+2)*pixelsPerSquare;
        ods.prepImage(w, h, fontSize);

        //draw a grid on it
        drawGrid(w, h);

        //reset array used to prevent overlapping trails
        for (int x = 0; x < worldHorizontal; x++)
            for (int y = 0; y < worldVertical; y++)
                blank[x][y] = true;

        //draw a square for a starting symbol
        int x1 = cpos(pos.x);
        int y1 = cpos(pos.y);
        ods.getGra().fillRect(
            x1-pixelsArrowHeight/2, y1-pixelsArrowHeight/2,
            pixelsArrowHeight, pixelsArrowHeight);
        drawCounter(pos.x, pos.y, false);
        blank[pos.x][pos.y] = false;
    }

    //enable or disable graphic logging
    public void drawOn(GPDrawing ods, String fname) throws IOException {
        if (ods == null) {
            //save completed image
            if (this.ods != null) {
                this.ods.writeGif(this.fname+gifCounter+".gif");
                this.ods = null;
            }
        } else {
            //prepare for drawing on image
            this.ods = ods;
            this.fname = fname;
            gifCounter = 1;
            moveCounter = 0;
            reset();
            prepImage();
        }
    }
}
