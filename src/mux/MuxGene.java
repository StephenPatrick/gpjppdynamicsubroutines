// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import gpjpp.*;

//extend GPGene to evaluate multiplexing

public class MuxGene extends GPGene {

    //public null constructor required during stream loading only
    public MuxGene() {}

    //this constructor called when new genes are created
    MuxGene(GPNode gpo) { super(gpo); }

    //this constructor called when genes are cloned during reproduction
    MuxGene(MuxGene gpo) { super(gpo); }

    //called when genes are cloned during reproduction
    protected Object clone() { return new MuxGene(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGENEID; }

    //must override GPGene.createChild to create MuxGene instances
    public GPGene createChild(GPNode gpo) { return new MuxGene(gpo); }

    //called by MuxGP.evaluate() for main branch of each GP
    //works with or without ADFs
    boolean evaluate(MuxVariables cfg, MuxGP gp) {

        int val = node.value();

        //evaluate the address and data terminals first
        if ((val >= MuxVariables.A0) && 
            (val < MuxVariables.A0+cfg.Addresses))
            return (cfg.input & 
                    (1 << (val-MuxVariables.A0))) != 0?
                        true : false;

        if ((val >= MuxVariables.D0) && 
            (val < MuxVariables.D0+cfg.Datalines))
            return (cfg.input & 
                    (1 << (cfg.Addresses+val-MuxVariables.D0))) != 0?
                        true : false;

        switch (val) {

            case MuxVariables.AND:
                //return AND of two arguments
                return ((MuxGene)get(0)).evaluate(cfg, gp) &&
                    ((MuxGene)get(1)).evaluate(cfg, gp);

            case MuxVariables.OR:
                //return OR of two arguments
                return ((MuxGene)get(0)).evaluate(cfg, gp) ||
                    ((MuxGene)get(1)).evaluate(cfg, gp);

            case MuxVariables.NOT:
                return !((MuxGene)get(0)).evaluate(cfg, gp);

            case MuxVariables.IF:
                //evaluate first argument
                if (((MuxGene)get(0)).evaluate(cfg, gp))
                    //if true return second argument
                    return ((MuxGene)get(1)).evaluate(cfg, gp);
                else
                    //else return third argument
                    return ((MuxGene)get(2)).evaluate(cfg, gp);

            case MuxVariables.ADF0:
                //return result of ADF0
                return ((MuxGene)(gp.get(1))).evaluate(cfg, gp);

            case MuxVariables.ADF1:
                //return result of ADF1
                return ((MuxGene)(gp.get(2))).evaluate(cfg, gp);
            
            default:
                throw new RuntimeException("Undefined function type "+node.value());
        }
    }

}
