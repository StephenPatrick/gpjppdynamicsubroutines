// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import java.util.Properties;
import gpjpp.*;

//extension of GPVariables for multiplex-specific stuff

public class MuxVariables extends GPVariables {

    public final static int AND = 0;
    public final static int OR = 1;
    public final static int NOT = 2;
    public final static int IF = 3;
    public final static int ADF0 = 4;
    public final static int ADF1 = 5;
    public final static int A0 = 6;
    public final static int D0 = 10;

    public int Addresses = 3;   //number of addresses to the mux
    public int Datalines = 8;   //number of datalines = 1 << Addresses
    public int input;           //passes input values to GP

    //public null constructor required for stream loading
    public MuxVariables() { /*gets default values*/ }

    //variables are never cloned in standard runs
    //public MuxVariables(MuxVariables gpo) {
    //    super(gpo);
    //    ...
    //}
    //protected Object clone() { return new MuxVariables(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERVARIABLESID; }

    //get values from properties
    public void load(Properties props) {

        if (props == null)
            return;
        super.load(props);
        Addresses = getInt(props, "Addresses", Addresses);
        if ((Addresses < 1) || (Addresses > 4))
            throw new RuntimeException("Addresses must be in range 1..4");
        Datalines = 1 << Addresses;
    }

    //get values from a stream
    protected void load(DataInputStream is)
        throws ClassNotFoundException, IOException,
            InstantiationException, IllegalAccessException {

        super.load(is);
        Addresses = is.readInt();
        Datalines = 1 << Addresses;
    }

    //save values to a stream
    protected void save(DataOutputStream os) throws IOException {

        super.save(os);
        os.writeInt(Addresses);
    }

    //write values to a text file
    public void printOn(PrintStream os, GPVariables cfg) {

        super.printOn(os, cfg);
        os.println("Addresses                 = "+Addresses);
    }
}
