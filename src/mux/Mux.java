// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import java.util.Properties;
import gpjpp.*;

//Muxmower test
class Mux extends GPRun {

    //must override GPRun.createVariables to return mux-specific variables
    protected GPVariables createVariables() { 
        return new MuxVariables(); 
    }

    //add the multiplexer inputs as terminals
    protected void addTerminals(GPVariables cfg, GPNodeSet ns) {
        
        for (int i = 0; i < ((MuxVariables)cfg).Addresses; i++)
            ns.putNode(new GPNode(MuxVariables.A0+i, "A"+i));
        for (int i = 0; i < ((MuxVariables)cfg).Datalines; i++)
            ns.putNode(new GPNode(MuxVariables.D0+i, "D"+i));
    }

    //add the main logic functions
    protected void addLogic(GPVariables cfg, GPNodeSet ns) {

        ns.putNode(new GPNode(MuxVariables.AND, "and", 2));
        ns.putNode(new GPNode(MuxVariables.OR, "or", 2));
        ns.putNode(new GPNode(MuxVariables.NOT, "not", 1));
        ns.putNode(new GPNode(MuxVariables.IF, "if", 3));
    }

    //must override GPRun.createNodeSet to return 
    //  initialized set of functions & terminals
    protected GPAdfNodeSet createNodeSet(GPVariables cfg) {

        int terminals = ((MuxVariables)cfg).Addresses+
            ((MuxVariables)cfg).Datalines;

        GPAdfNodeSet adfNs = new GPAdfNodeSet();


        if (cfg.UseADFs) {
            adfNs.reserveSpace(3);
            GPNodeSet ns0 = new GPNodeSet(6+terminals);
            GPNodeSet ns1 = new GPNodeSet(4+terminals);
            GPNodeSet ns2 = new GPNodeSet(5+terminals);
            adfNs.put(0, ns0);
            adfNs.put(1, ns1);
            adfNs.put(2, ns2);

            //RPB
            addTerminals(cfg, ns0);
            addLogic(cfg, ns0);
            ns0.putNode(new GPNode(MuxVariables.ADF0, "adf0"));
            ns0.putNode(new GPNode(MuxVariables.ADF1, "adf1"));
  
            //ADF0
            addTerminals(cfg, ns1);
            addLogic(cfg, ns1);
  
            //ADF1
            addTerminals(cfg, ns2);
            addLogic(cfg, ns2);
            ns2.putNode(new GPNode(MuxVariables.ADF0, "adf0"));

        } else {
            adfNs.reserveSpace(1);
            GPNodeSet ns0 = new GPNodeSet(4+terminals);
            adfNs.put(0, ns0);

            //RPB
            addTerminals(cfg, ns0);
            addLogic(cfg, ns0);
        }

        return adfNs;
    }

    //must override GPRun.createPopulation to return 
    //  mux-specific population
    protected GPPopulation createPopulation(GPVariables cfg, 
        GPAdfNodeSet adfNs) {
        return new MuxPopulation(cfg, adfNs);
    }

    //construct this test case
    Mux(String baseName) { super(baseName, true); }

    //main application function
    public static void main(String[] args) {

        //compute base file name from command line parameter
        String baseName;
        if (args.length == 1)
            baseName = args[0];
        else
            baseName = "mux2";

        //construct the test case
        Mux test = new Mux(baseName);

        //run the test
        test.run();

        //make sure all threads are killed
        System.exit(0);
    }
}
