// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import gpjpp.*;

//extend GP for multiplexing
//class must be public for stream loading; otherwise non-public ok

public class MuxGP extends GP {

    //public null constructor required during stream loading only
    public MuxGP() {}

    //this constructor called when new GPs are created
    MuxGP(int genes) { super(genes); }

    //this constructor called when GPs are cloned during reproduction
    MuxGP(MuxGP gpo) { super(gpo); }

    //called when GPs are cloned during reproduction
    protected Object clone() { return new MuxGP(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGPID; }

    //must override GP.createGene to create MuxGene instances
    public GPGene createGene(GPNode gpo) { return new MuxGene(gpo); }

    //must override GP.evaluate to return standard fitness
    public double evaluate(GPVariables cfg) {

        MuxVariables mcfg = (MuxVariables)cfg;

        int testcases = 1 << (mcfg.Addresses+mcfg.Datalines);
        int correct = 0;

        //test all possible inputs to multiplexer
        for (int i = 0; i < testcases; i++) {
            //pass the terminal values into the GP via the input field
            mcfg.input = i;

            //evaluate main tree
            boolean answer = ((MuxGene)get(0)).evaluate(mcfg, this);

            int add = i & (mcfg.Datalines-1);
            boolean expected = ((i >> mcfg.Addresses) & (1 << add)) != 0;
            if (answer == expected)
                correct++;
        }

        //fitness is the number of test cases missed (best = 0.0)
        double stdF = testcases-correct;

        if (cfg.ComplexityAffectsFitness)
            //add length into fitness to promote small trees
            stdF += length()/1000.0;

        //return standard fitness
        return stdF;
    }

    //optionally override GP.printOn to show lawn-specific data
    public void printOn(PrintStream os, GPVariables cfg) {

        super.printOn(os, cfg);

        //display correctness grid
        MuxVariables mcfg = (MuxVariables)cfg;
        int testcases = 1 << (mcfg.Addresses+mcfg.Datalines);
        for (int i = 0; i < testcases; i++) {
            mcfg.input = i;
            boolean answer = ((MuxGene)get(0)).evaluate(mcfg, this);

            int add = i & (mcfg.Datalines-1);
            boolean expected = ((i >> mcfg.Addresses) & (1 << add)) != 0;
            if (answer == expected)
                os.print("*");
            else
                os.print(".");
            if ((i & 63) == 63)
                os.println();
        }
    }
}
