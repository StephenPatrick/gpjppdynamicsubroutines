package gpjpp;

import java.util.ArrayList;

public class GPSubroutine extends GPNode {
    
    // The tree representation of the function that this node executes
    protected GPGene tree;
    protected boolean filled;
    
    public GPSubroutine(int nVal, String str, int args, GPGene subtree) {
        nodeValue = nVal;
        representation = new String(str);
        numOfArgs = args;
        tree = subtree;
        filled = false;
    }
    
    public int length(){
        return tree.length();
    }

    public void addChildren(ArrayList<GPObject> nodes){
        
        if (numOfArgs > nodes.size())
            throw new RuntimeException("Insufficient arguments for node: " + representation);
        if (numOfArgs < nodes.size())
            throw new RuntimeException("Too many arguments for node: " + representation);
        
        int i = 0;
        while (i < numOfArgs){
            GPGene functionToFill = tree.findNextEmptyFunction();
            if (functionToFill == null){
                System.out.println(i);
                tree.printOn(System.out,null);
                System.out.println("numOfArgs = " + numOfArgs);
                throw new RuntimeException("Empty function not found for node: " + representation);
            }
            for (int j = 0; j < functionToFill.node.arguments(); j++){
                functionToFill.put(j,(GPGene)nodes.get(i));
                i++;
            }
        }
        filled = true;
    }

    public boolean isFull(){return filled;}

    public GPGene getTree(){ return tree;}

    public boolean equals(GPSubroutine other){
        return tree.equals(other.getTree());
    }

    public boolean equals(GPGene other){
        return tree.equals(other);
    }
}
