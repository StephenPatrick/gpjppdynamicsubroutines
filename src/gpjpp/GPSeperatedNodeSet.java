package gpjpp;

import java.io.*;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Stores information about the functions and terminals used in 
 * one tree branch of a particular genetic programming problem.<p>
 *
 * GPSeperatedNodeSet is distinguished from GPNodeSet initially
 * in that it stores terminals and functions in seperate containers.
 * It permits adding functions to the NodeSet throughout the runtime
 * of a GP to let evolved subroutines speed up the evolution of the
 * population.
 *
 * @version 0.π
 */
public class GPSeperatedNodeSet extends GPNodeSet{

    /**
     * Seperate container from the inherited container to store terminals.
     */
    protected GPContainer terminalContainer;

    /**
     * The limit on functions this set's container will hold at once.
     */
    protected final int maxSize = 100;

    /**
     * An array representing the weighted value of each function.
     */
    protected int[] functionWeights;

    /**
     * An array for roulette search representing cumulative weights 
     * of the function set in order.
     */
    protected int[] overallWeights;

    /**
     * A Hashtable storing each function as a string for matching against
     * eachother. 
     */
    protected Hashtable<String,Boolean> subroutineHash;

    protected int nextFunctionVal;
    
    /**
     * Constructor, takes intial size of containers
     * 
     * @param functionCount  The initial number of functions stored.
     * @param terminalCount  The initial number of terminals stored.
     */
    public GPSeperatedNodeSet(int functionCount, int terminalCount) {
        super(functionCount);
        terminalContainer = new GPContainer(terminalCount);
        numFunctions = 0;
        nextFunctionVal = terminalCount;
        numTerminals = 0;
        functionWeights = new int[maxSize];
        overallWeights = new int[maxSize];
        subroutineHash = new Hashtable<String,Boolean>();
    }

    /**
     * Add a function to this nodeset
     *
     * @param function  A GPNode which is presumed to be a function
     */
    public void putFunction(GPNode function){
        putFunction(function, 2); //Magic number alert
    }

    public void putFunction(GPNode function, int weight){
        if (numFunctions == maxSize){
            removeUnusedFunctions();
        }
        functionWeights[numFunctions] = weight;
        put(numFunctions, function);
        if (numFunctions != 0){
            overallWeights[numFunctions] = overallWeights[numFunctions-1] + weight;
        }
        else
            overallWeights[numFunctions] = weight;
        numFunctions++;
        nextFunctionVal++;
        if (function instanceof GPSubroutine)
            subroutineHash.put(((GPSubroutine)function).getTree().stringValue(),true);
        
    }

    /**
     * Add a terminal to this nodeset
     * One shouldn't add to this past terminalCount.
     */ 
    public void putTerminal(GPNode terminal){
        terminalContainer.put(numTerminals, terminal);
        numTerminals++;
    }

    /**
     * Add a terminal or function to this nodeset.
     */

    public void putNode(GPNode gpo) {
       
        //cannot duplicate node with same identification number
        if (searchForNode(gpo.value()) != null)
            throw new RuntimeException("Cannot duplicate node ID in node set");

        //Put functions and terminals in their seperate Containers
        if (gpo.isFunction())
            putFunction(gpo);
        else
            putTerminal(gpo);
    }
    
    /**
     * Returns the GPNode in this set that has the specified node
     * value, or null if none is found. For internal use.
     */
    protected GPNode searchForNode(int value) {
        for (int i = 0; i < numFunctions; i++) {
            GPNode current = (GPNode)get(i);
            if ((current != null) && (current.value() == value))
                return current;
        }
        for (int i = 0; i < numTerminals; i++) {
            GPNode current = (GPNode)terminalContainer.get(i);
            if ((current != null) && (current.value() == value))
                return current;
        }
        return null;
    }

    /**
     * Returns a random function node from this set. Used by
     * <a href="gpjpp.GP.html#create">GP.create()</a> and 
     * <a href="gpjpp.GPGene.html#create">GPGene.create()</a> 
     * to build new trees.
     */
    public GPNode chooseFunction() {
        return (GPNode)get(rouletteSearch(overallWeights,numFunctions-1));
    }

    /**
     * Returns a random terminal node from this set. Used by
     * <a href="gpjpp.GPGene.html#create">GPGene.create()</a> 
     * to build new trees.
     */
    public GPNode chooseTerminal() {
        return (GPNode)terminalContainer.get(GPRandom.nextInt(numTerminals));
    }

    /**
     * Returns a random node from this set with the specified
     * number of arguments. Returns null if there is no such
     * node. Used for 
     * <a href="gpjpp.GP.html#swapMutation">swap mutation</a>.
     */
    public GPNode chooseNodeWithArgs(int args) {

        ArrayList<Integer> foundNodes = new ArrayList<Integer>();
        
        //count all nodes that have the specified number of arguments
        for (int i = 0; i < numFunctions; i++) {
            GPNode n = (GPNode)get(i);
            if (n != null)
               if (n.arguments() == args)
                    foundNodes.add(i);
        }
        
        if (args == 0){
            //Short circuit the rest of the function for arg ==0
            return chooseTerminal();
        	//for (int i = -1; i > ((-1)*numTerminals) - 1; i--){
			//	foundNodes.add(i);
			//}
        }

        int[] totalWeights = new int[foundNodes.size()];
        if (foundNodes.get(0) < 0)
        	totalWeights[0] = 2;
        else
        	totalWeights[0] = functionWeights[foundNodes.get(0)];
        for (int i = 1; i < foundNodes.size(); i++){
        	if (foundNodes.get(i) < 0)
        		totalWeights[i] = totalWeights[i-1] + 2;
        	else
            	totalWeights[i] = totalWeights[i-1] + functionWeights[foundNodes.get(i)];
        }
        int index = rouletteSearch(totalWeights,totalWeights.length-1);
             
        if (foundNodes.get(index) < 0)
        	return (GPNode)terminalContainer.get(-1*foundNodes.get(index));
        return (GPNode)get(foundNodes.get(index));
    }

    /*
     * Clear out unused, automatically defined functions.
     * Lets have this remove half of them.
     */
    private void removeUnusedFunctions(){
        //get average weight
        int totalWeight = 0;
        for (int j=0; j<numFunctions; j++){
            totalWeight+=functionWeights[j];
        }
        int avgWeight = totalWeight/numFunctions;
        
        //for each of the functions:
        for (int i=0; i<numFunctions; i++){
        //check if it's automatically defined; if it is, check if it has above average weight
            GPObject fun = get(i);
            if (fun instanceof GPSubroutine && !(functionWeights[i] > avgWeight)){
                //if not used, remove
                Boolean returnVal = subroutineHash.put(((GPSubroutine)fun).getTree().stringValue(),false);
				remove(i);
                i--;
                numFunctions--;
            }
        }
    }

    /*
     * Roulette search for random function selection
     */
    private int rouletteSearch(int[] weights, int initalMax){
        int searchFor = GPRandom.nextInt(weights[initalMax]);
        int mid = initalMax / 2;
        int max = initalMax;
        int min = 0;
        while (min < max){
            if (weights[mid] >= searchFor){
                if (mid == 0 || weights[mid-1] < searchFor){
                    return mid;
                }
                max = mid-1;
                mid = (max + min) / 2;
            }
            else {
                if (mid == initalMax || weights[mid+1] > searchFor){
                    return mid;
                }
                min = mid+1;
                mid = (max + min) / 2;
            }
        }
        return mid;
    }

    public int nextFunctionVal(){
        return nextFunctionVal + 1;
    }

    public boolean containsSubroutine(GPGene subroutine){
        if (subroutineHash.get(subroutine.stringValue()) == null)
        	return false;
        return subroutineHash.get(subroutine.stringValue());
    }

    public void setFunctionWeights(Double[] newFunctionWeights){
        if (get(0) instanceof GPSubroutine){
            functionWeights[0] = (int)Math.round(newFunctionWeights[0]);
            overallWeights[0] = (int)Math.round(newFunctionWeights[0]);
        }
        for (int i = 1; i < numFunctions; i++){
            if (get(i) instanceof GPSubroutine){
                functionWeights[i] = (int)Math.round(newFunctionWeights[i]);
                overallWeights[i] = overallWeights[i-1] + (int)Math.round(newFunctionWeights[i]);
            }
        }
    }
    
    public int highestFunctionWeight(){
        int returnVal = 2;
        for (int i = 0; i < numFunctions; i++){
               if (functionWeights[i] > returnVal)
                   returnVal = functionWeights[i];
        }
        return returnVal;
    }
}
