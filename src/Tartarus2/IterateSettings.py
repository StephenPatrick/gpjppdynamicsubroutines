import os
import fileinput
import re
import sys

#no need to recompile
#os.system('javac -cp "../:." *.java')

#define settings to iterate over
#note: change manually for more things being altered, currently set to three
setsToChange = ["SubroutinesPerGeneration", "MaxSubroutineDepth", "MaxSubroutineArgs"]
modeMatrix = [["1", "10", "50"], ["2", "3", "10"], ["3", "4","8"]]

#note: things listed above are simply deleted from this list
#incorporate variations on subroutines
#create a value combination
for m1 in modeMatrix[0]:
    for m2 in modeMatrix[1]:
        for m3 in modeMatrix[2]:
            modes = [m1, m2, m3]
            for i in range(5):
                print modes
                with open("tartarus.ini", "w") as ini:
                    ini.write(
                    """PopulationSize            = 250
NumberOfGenerations       = 100
CrossoverProbability      = 70.0
CreationProbability       = 0.0
CreationType              = Grow
MaximumDepthForCreation   = 8
MaximumDepthForCrossover  = 16
MaximumComplexity         = 100
SelectionType             = Tournament
TournamentSize            = 5
DemeticGrouping           = true
DemeSize                  = 50
DemeticMigProbability     = 75.0
SwapMutationProbability   = 10.0
ShrinkMutationProbability = 10.0
TerminationFitness        = 0.0
GoodRuns                  = 0
AddBestToNewPopulation    = true
SteadyState               = false
PrintDetails              = false
PrintPopulation           = false
PrintExpression           = true
PrintTree                 = false
TreeFontSize              = 12
UseDynamicSubroutines     = true     
SubroutineSelectionMethod = Random
""")

                    #actually write the option changes
                    for i in range(0, 3):
                        setting = setsToChange[i]
                        ini.write(setting+" = "+modes[i]+"\n")

                    ini.write(
                    """UseADFs                   = false
TestDiversity             = false
ComplexityAffectsFitness  = true
CheckpointGens            = 0
WorldHorizontal           = 6
WorldVertical             = 6
NumTestGrids              = 20
NumBlocks                 = 5
NumSteps                  = 80
                    """
                        )
                writefile = open("file.txt","a")
                toAppend = ""
                for line in modes:
                    toAppend += line+","
                writefile.write(toAppend)
                os.system('java -cp "../:." Tartarus >> file.txt')




