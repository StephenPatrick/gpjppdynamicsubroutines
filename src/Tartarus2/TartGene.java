// Tartarus Implementation
// Copyright (c) 2013, Sherri Goings
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//

import java.awt.Point;
import java.io.*;
import gpjpp.*;

//extend GPGene to evaluate Tartarus

public class TartGene extends GPGene {
    //public null constructor required during stream loading only
    public TartGene() {}

    //this constructor called when new genes are created
    TartGene(GPNode gpo) { super(gpo); }

    //this constructor called when genes are cloned during reproduction
    TartGene(TartGene gpo) { super(gpo); }

    //called when genes are cloned during reproduction
    protected Object clone() { return new TartGene(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGENEID; }

    //must override GPGene.createChild to create TartGene instances
    public GPGene createChild(GPNode gpo) { return new TartGene(gpo); }

    //called by TartGP.evaluate() for main branch of each GP
    int evaluate(TartVariables cfg, TartGP gp) {
        return evaluate(cfg, gp, null);
    }

    //called by TartGP.evaluate() for main branch of each GP
    int evaluate(TartVariables cfg, TartGP gp, PrintStream os) {
        
        if (cfg.dozerGrid.getSteps() > 80){
            return 0;   
        }
        
        if (node instanceof GPSubroutine){
            if (!( (GPSubroutine) node).isFull())
                ( (GPSubroutine) node).addChildren(container);
            ( (TartGene)(node.getTree().get(0)) ).evaluate(cfg, gp, os);
        }
    
        int val = node.value();

        if (val<=2) {
            if (val == Grid.LFT) cfg.dozerGrid.left();
            else if (val==Grid.RGT) cfg.dozerGrid.right();
            else if (val==Grid.FWD) cfg.dozerGrid.forward();
            if (os!=null) cfg.dozerGrid.print(os);
            cfg.dozerGrid.incrementSteps(1);
            return 0;
        }
        
        if (val <=10) {
            int result = -1; 
            if (val==Grid.UR)
                result = cfg.dozerGrid.sensor(1, -1);
            else if (val==Grid.MR)
                result = cfg.dozerGrid.sensor(0, -1);
            else if (val==Grid.LR)
                result = cfg.dozerGrid.sensor(-1, -1);
            else if (val==Grid.UM)
                result = cfg.dozerGrid.sensor(1, 0);
            else if (val==Grid.LM)
                result = cfg.dozerGrid.sensor(-1, 0);
            else if (val==Grid.UL)
                result = cfg.dozerGrid.sensor(1, 1);
            else if (val==Grid.ML)
                result = cfg.dozerGrid.sensor(0, 1);
            else if (val==Grid.LL)
                result = cfg.dozerGrid.sensor(-1, 1);
			//System.out.println("result = " + result);
            ( (TartGene)get(result) ).evaluate(cfg, gp, os);
            return 1;
        }
        
        if (val==Grid.PRG2) {
            ( (TartGene)get(0) ).evaluate(cfg, gp, os);
            ( (TartGene)get(1) ).evaluate(cfg, gp, os);
        }
        
        if (val==Grid.PRG3){
            ( (TartGene)get(0) ).evaluate(cfg, gp, os);
            ( (TartGene)get(1) ).evaluate(cfg, gp, os);
            ( (TartGene)get(2) ).evaluate(cfg, gp, os);
        }

        if (val==Grid.RAND){
            int rand = cfg.dozerGrid.randInt();
            if (rand <= 5){
                ( (TartGene)get(0) ).evaluate(cfg, gp, os);
            }
            else {
                ( (TartGene)get(1) ).evaluate(cfg, gp, os);
            }
            return 1;
        }
        
        return 2;
    }
}


