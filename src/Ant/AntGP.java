// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import gpjpp.*;

//extend GP for ant trees
//class must be public for stream loading; otherwise non-public ok

public class AntGP extends GP {

    //public null constructor required during stream loading only
    public AntGP() {}

    //this constructor called when new GPs are created
    AntGP(int genes) { super(genes); }

    //this constructor called when GPs are cloned during reproduction
    AntGP(AntGP gpo) { super(gpo); }

    //called when GPs are cloned during reproduction
    protected Object clone() { return new AntGP(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGPID; }

    //must override GP.createGene to create AntGene instances
    public GPGene createGene(GPNode gpo) { return new AntGene(gpo); }

    //must override GP.evaluate to return standard fitness
    public double evaluate(GPVariables cfg) {

        AntVariables acfg = (AntVariables)cfg;

        //create ant first time through
        if (acfg.ant == null)
            acfg.createAnt();

        //prepare ant for moving
        acfg.ant.reset();

        int foodEaten = 0;

        //evaluate result-producing branch while energy and food remain
        while ((acfg.ant.energy > 0) && (foodEaten < acfg.ant.maxFood))
            foodEaten += ((AntGene)get(0)).evaluate(acfg);

        acfg.ant.foodMissed = acfg.ant.maxFood-foodEaten;
        double stdF = acfg.ant.foodMissed;

        if (cfg.ComplexityAffectsFitness)
            //add length into fitness to promote small trees
            stdF += length()/1000.0;

        //return standard fitness
        return stdF;
    }

    //optionally override GP.printOn to show ant-specific data
    public void printOn(PrintStream os, GPVariables cfg) {

        super.printOn(os, cfg);

        //re-evaluate ant to get its trail array
        evaluate(cfg);

        //print ant-specific data
        AntVariables acfg = (AntVariables)cfg;
        os.println("food missed:      "+acfg.ant.foodMissed);
        os.println("energy remaining: "+acfg.ant.energy);
        acfg.ant.printTrail(os);
    }
}
