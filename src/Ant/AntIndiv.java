// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import java.awt.Point;
import gpjpp.*;

//keep track of an ant individual's state while evaluating fitness
//not part of the GP hierarchy, but called upon in AntGP and AntGene

public final class AntIndiv {

    //functions and terminals
    public final static int LEFT = 0;       //turn left
    public final static int RIGHT = 1;      //turn right
    public final static int FORWARD = 2;    //move forward
    public final static int IFFOODAHEAD = 3;//if food is ahead execute #0, else #1
    public final static int PROG2 = 4;      //execute two arguments, return sum
    public final static int PROG3 = 5;      //execute three ...
    public final static int PROG4 = 6;      //execute four ...

    //characters in trail file
    public final static byte NOTHING = (byte)'.';
    public final static byte FOOD = (byte)'X';
    public final static byte GAP = (byte)'O';

    //size of the trail
    public static int worldHorizontal;
    public static int worldVertical;

    //the world the ant is moving in
    protected byte[][] constantWorld;       //loaded from trail file
    protected byte[][] world;               //updated as ant moves

    //the trail left by the ant
    protected byte[][] trail;
    protected int trailCounter;

    //current position of ant
    protected Point pos = new Point(0, 0);      //current position
    protected Point tmpPos = new Point(0, 0);   //temp for forwardPosition

    //direction the ant is facing (value 0-3) and energy left to move
    protected int direction;
    protected int energy;

    //starting energy
    public int maxEnergy;

    //maximum amount of food on trail; set in readTrail
    public int maxFood;

    //number of food pellets missed, calculated by AntGP.Evaluate
    public int foodMissed;

    public AntIndiv(int worldH, int worldV, int energy, String trailName) 
        throws EOFException, FileNotFoundException, IOException {

        if ((worldH < 1) || (worldV < 1))
            throw new RuntimeException("Invalid world size");
        worldHorizontal = worldH;
        worldVertical = worldV;
        maxEnergy = energy;

        world = new byte[worldHorizontal][worldVertical];
        constantWorld = new byte[worldHorizontal][worldVertical];
        trail = new byte[worldHorizontal][worldVertical];

        readTrail(trailName);
    }

    public final void readTrail(String fname)
        throws EOFException, FileNotFoundException, IOException {

        //open trail file
        DataInputStream ifs = new DataInputStream(
            new FileInputStream(fname));

        byte ch;
        int x = 0;
        int y = 0;
        maxFood = 0;

        //move to first '#' character, indicating start of map
        while ((ch = ifs.readByte()) != '#') ;

        //read map
        while (y < worldVertical) {
            //skip cr/lf
            while (((ch = ifs.readByte()) == '\r') || (ch == '\n')) ;
            if ((ch != NOTHING) && (ch != FOOD) && (ch != GAP))
                throw new RuntimeException("Bad character in trail file");
            constantWorld[x][y] = ch;
            if (constantWorld[x][y] == FOOD)
                maxFood++;
            x++;
            if (x == worldHorizontal) {
                y++;
                x = 0;
            }
        }

        //see if enough characters were read
        if ((y < worldVertical) || (x != 0))
            throw new RuntimeException("Trail file doesn't match expected size");

        ifs.close();
    }

    final void reset() {
        //reset world and trail
        for (int x = 0; x < worldHorizontal; x++)
            for (int y = 0; y < worldVertical; y++) {
                world[x][y] = constantWorld[x][y];
                if (constantWorld[x][y] == GAP)
                    trail[x][y] = NOTHING;
                else
                    trail[x][y] = constantWorld[x][y];
            }

        energy = maxEnergy;
        direction = 0;     //East
        pos.x = 0;         //Northwest corner of world
        pos.y = 0;
        trailCounter = 0;
        updateTrail();
    }

    public final void left() {
        if (energy <= 0)
            return;
        energy--;
        direction = (direction+1) % 4;
    }

    public final void right() {
        if (energy <= 0)
            return;
        energy--;
        direction = (direction+3) % 4;
    }

    public final int forward() {
        if (energy <= 0)
            return 0;
        energy--;

        //move to new position
        forwardPosition();
        pos.x = tmpPos.x;
        pos.y = tmpPos.y;

        //update trail map
        updateTrail();

        //eat food, if something is there
        if (world[pos.x][pos.y] == FOOD) {
            world[pos.x][pos.y] = NOTHING;
            return 1;
        } else
            return 0;
    }

    //computes new x and y position in tmpPos
    //ant wraps at map edges
    protected final void forwardPosition() {
        tmpPos.x = pos.x;
        tmpPos.y = pos.y;

        switch (direction) {
            case 0: //East
                if (tmpPos.x < worldHorizontal-1)
                    tmpPos.x++;
                else
                    tmpPos.x = 0;
                break;

            case 1: //North
                if (tmpPos.y > 0)
                    tmpPos.y--;
                else
                    tmpPos.y = worldVertical-1;
                break;

            case 2: // West
                if (tmpPos.x > 0)
                    tmpPos.x--;
                else
                    tmpPos.x = worldHorizontal-1;
                break;

            case 3: // South
                if (tmpPos.y < worldVertical-1)
                    tmpPos.y++;
                else
                    tmpPos.y = 0;
                break;

            default:
                throw new RuntimeException("Bad direction");
        }
    }

    public final boolean isFoodAhead() {
        forwardPosition();
        return (world[tmpPos.x][tmpPos.y] == FOOD);
    }

    //store cycling sequential numbers 0..9 along trail of ant
    protected final void updateTrail() {
        trail[pos.x][pos.y] = (byte)((byte)'0'+(trailCounter++ % 10));
    }

    public final void printTrail(PrintStream os) {
        for (int y = 0; y < worldVertical; y++) {
            //print path of ant
            for (int x = 0; x < worldHorizontal; x++)
                os.print((char)trail[x][y]);

            //print the initial world next to it for reference
            os.print("  ");
            for (int x = 0; x < worldHorizontal; x++)
                os.print((char)constantWorld[x][y]);

            os.println();
        }
    }
}
