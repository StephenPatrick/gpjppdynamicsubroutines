// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import gpjpp.*;

//Artificial ant test
class Ant extends GPRun {

    //must override GPRun.createVariables to return ant-specific variables
    protected GPVariables createVariables() { 
        return new AntVariables(); 
    }

    //must override GPRun.createNodeSet to return 
    //  initialized set of functions & terminals
    protected GPAdfNodeSet createNodeSet(GPVariables cfg) {
        GPAdfNodeSet adfNs = new GPAdfNodeSet();

        //no ADFs used regardless of cfg setting
        adfNs.reserveSpace(1);

        //add an extra function for big tests like los altos
        boolean bigTest = (((AntVariables)cfg).WorldHorizontal > 32);
        GPNodeSet ns;
        if (bigTest)
            ns = new GPNodeSet(7);
        else
            ns = new GPNodeSet(6);
        adfNs.put(0, ns);

        ns.putNode(new GPNode(AntIndiv.LEFT, "left"));
        ns.putNode(new GPNode(AntIndiv.RIGHT, "right"));
        ns.putNode(new GPNode(AntIndiv.FORWARD, "forward"));
        ns.putNode(new GPNode(AntIndiv.IFFOODAHEAD, "iffoodahead", 2));
        ns.putNode(new GPNode(AntIndiv.PROG2, "prog2", 2));
        ns.putNode(new GPNode(AntIndiv.PROG3, "prog3", 3));
        if (bigTest)
            ns.putNode(new GPNode(AntIndiv.PROG4, "prog4", 4));

        return adfNs;
    }

    //must override GPRun.createPopulation to return 
    //  ant-specific population
    protected GPPopulation createPopulation(GPVariables cfg, 
        GPAdfNodeSet adfNs) {
        return new AntPopulation(cfg, adfNs);
    }

    //construct this test case
    Ant(String baseName) { super(baseName, true); }

    //main application function
    public static void main(String[] args) {

        //compute base file name from command line parameter
        String baseName;
        if (args.length == 1)
            baseName = args[0];
        else
            baseName = "santafe";

        //construct the test case
        Ant test = new Ant(baseName);

        //run the test
        test.run();

        //make sure all threads are killed
        System.exit(0);
    }
}
