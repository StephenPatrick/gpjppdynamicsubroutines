// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import gpjpp.*;

//extend GPGene to evaluate ants

public class AntGene extends GPGene {

    //public null constructor required during stream loading only
    public AntGene() {}

    //this constructor called when new genes are created
    AntGene(GPNode gpo) { super(gpo); }

    //this constructor called when genes are cloned during reproduction
    AntGene(AntGene gpo) { super(gpo); }

    //called when genes are cloned during reproduction
    protected Object clone() { return new AntGene(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGENEID; }

    //must override GPGene.createChild to create AntGene instances
    public GPGene createChild(GPNode gpo) { return new AntGene(gpo); }

    //called by AntGP.evaluate() for main branch of each GP
    //  returns food eaten by ant
    //  Java guarantees left-to-right evaluation order, 
    //  hence progn expressions below
    int evaluate(AntVariables cfg) {

        switch (node.value()) {

            case AntIndiv.LEFT:
                //turn left and return 0
                cfg.ant.left();
                return 0;

            case AntIndiv.RIGHT:
                //turn right and return 0
                cfg.ant.right();
                return 0;

            case AntIndiv.FORWARD:
                //move forward and return food eaten
                return cfg.ant.forward();

            case AntIndiv.IFFOODAHEAD:
                //evaluate first branch if food ahead, else second branch
                if (containerSize() != 2)
                    throw new RuntimeException("IFA doesn't have two arguments");
                if (cfg.ant.isFoodAhead())
                    return ((AntGene)get(0)).evaluate(cfg);
                else
                    return ((AntGene)get(1)).evaluate(cfg);

            case AntIndiv.PROG2:
                //evaluate two children and return total food eaten
                if (containerSize() != 2)
                    throw new RuntimeException("PROG2 doesn't have two arguments");
                return ((AntGene)get(0)).evaluate(cfg)+
                       ((AntGene)get(1)).evaluate(cfg);

            case AntIndiv.PROG3:
                //evaluate three children and return total food eaten
                if (containerSize() != 3)
                    throw new RuntimeException("PROG3 doesn't have three arguments");
                return ((AntGene)get(0)).evaluate(cfg)+
                       ((AntGene)get(1)).evaluate(cfg)+
                       ((AntGene)get(2)).evaluate(cfg);

            case AntIndiv.PROG4:
                //evaluate four children and return total food eaten
                if (containerSize() != 4)
                    throw new RuntimeException("PROG4 doesn't have four arguments");
                return ((AntGene)get(0)).evaluate(cfg)+
                       ((AntGene)get(1)).evaluate(cfg)+
                       ((AntGene)get(2)).evaluate(cfg)+
                       ((AntGene)get(3)).evaluate(cfg);

            default:
                throw new RuntimeException("Undefined function type "+node.value());
        }
    }
}
